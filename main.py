import sys
import logging
import requests
import shutil
import urllib.parse
import json
import telebot
import uuid
import pickle
import tempfile
import os

MAX_PRICE = 9999999

#####################################################
# CONFIGURATION #####################################
#####################################################

logging.basicConfig(level=logging.DEBUG)

# token = '1392147659:AAGXG1yXUy-mxyW7y97laNPDx0zCvaYc9SM'
token = sys.argv[1]

logging.debug(f'[CONFIG] Token: {token}')


#####################################################
# UTILS #############################################
#####################################################

def extract_args(text):
    idx = text.find(' ')
    if idx > -1:
        return text[idx:]
    else:
        return ''


def download_image(url, f_tmp_image):
    response = requests.get(url, stream=True)
    with f_tmp_image as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response


def generate_random_id():
    return uuid.uuid4().hex


#####################################################
# CLASSES ###########################################
#####################################################

class Kino:
    """
    Сущность "кино".
    """

    def __init__(self, title, url, poster_url, id):
        self.title = title
        self.id = id
        self.url = url
        self.poster_url = poster_url
        self.db_callback_data = None

    def __repr__(self):
        return f'Kino(title={self.title}; url={self.url}; poster_url={self.poster_url}; db_callback_data={self.db_callback_data})'


class KinoExt:
    title = None
    original_title = None
    description = None
    poster_url = None
    genres = None
    countries = None
    years = None


class PriceData:
    name = None
    price = None


class KinopoiskHD:
    """
    Механизм поиска по "КинопоискHD"
    """

    _query_url = 'https://api.ott.kinopoisk.ru/v13/suggested-data?query={}&withPersons=false'
    _film_url = 'https://hd.kinopoisk.ru/film/{}'
    _metadata_url = 'https://api.ott.kinopoisk.ru/v12/hd/content/{}/metadata'
    _price_url = 'https://api.ott.kinopoisk.ru/v12/purchase-options/{}?filterByStreams=false'

    @staticmethod
    def _get_text(url: str):
        r = requests.get(url)
        text = r.text
        return text

    def get_url_film(self, film_id):
        return self._film_url.format(film_id)

    def search(self, name: str):
        """
        Найти фильмы по наименованию
        :param name: полное или частичное наименование
        :return: список сущностей Kino
        """

        query = urllib.parse.quote_plus(name)
        url = self._query_url.format(query)
        logging.debug(f'[KinopoinsHD] Request url: {url}')
        raw_json = self._get_text(url)
        logging.debug(f'[KinopoinsHD] Response json: {raw_json}')

        list_films = json.loads(raw_json).get('films')
        if list_films is None:
            list_films = []

        result_list = []
        for item in list_films:
            kino = Kino(title=item.get('title'),
                        url=self._film_url.format(item.get('filmId')),
                        poster_url=item.get('posterUrl'),
                        id=item.get('filmId'))
            result_list.append(kino)
        return result_list

    def get_kino_data(self, film_id):
        url = self._metadata_url.format(film_id)
        logging.debug(f'[KinopoinsHD] Request url: {url}')
        raw_json = self._get_text(url)
        logging.debug(f'[KinopoinsHD] Response json: {raw_json}')

        json_object = json.loads(raw_json)
        kino_ext = KinoExt()
        kino_ext.title = json_object.get('title')
        kino_ext.original_title = json_object.get('originalTitle')
        kino_ext.description = json_object.get('description')
        kino_ext.poster_url = json_object.get('posterUrl')
        kino_ext.genres = json_object.get('genres')
        kino_ext.countries = json_object.get('countries')
        kino_ext.years = json_object.get('years')

        return kino_ext

    def get_price(self, film_id):
        url = self._price_url.format(film_id)
        logging.debug(f'[KinopoinsHD] Request url: {url}')
        raw_json = self._get_text(url)
        logging.debug(f'[KinopoinsHD] Response json: {raw_json}')

        json_array = json.loads(raw_json).get('options')
        result_price_list = []

        for item in json_array:
            price_data = PriceData()
            price_data.name = item.get('name')
            price_data.price = item.get('price')
            result_price_list.append(price_data)

        return result_price_list


class DbRow:
    text = None
    result = None


class Database:
    table1 = {}
    table2 = {}


#####################################################
# MAIN ##############################################
#####################################################

kinopoiskhd = KinopoiskHD()
database = Database()
bot = telebot.TeleBot(token)


# region Setup handlers
@bot.message_handler(commands=['start'])
def start_message(message: telebot.types.Message):
    pass


def callback_handler_kino_info(call):
    db_row_id2 = call.data[2:]
    dbdata = database.table2.get(db_row_id2)

    film_id = dbdata.split('|')[0]
    db_row_id = dbdata.split('|')[1]

    kino_ext = kinopoiskhd.get_kino_data(film_id)
    msg = f'<b>{kino_ext.title}</b>\n({kino_ext.original_title})\n\n'
    msg = msg + '<b>Жанр:</b> ' + ", ".join(kino_ext.genres) + '\n'
    msg = msg + '<b>Год:</b> ' + ', '.join(str(x) for x in kino_ext.years) + '\n'
    msg = msg + '<b>Страна:</b> ' + ", ".join(kino_ext.countries) + '\n\n'
    msg = msg + f'{kino_ext.description}\n\n- - - - - - - - - -\n\n'

    list_price = kinopoiskhd.get_price(film_id)
    msg2 = '- <b>Купить:</b> от {} руб'
    last_price = MAX_PRICE
    for price_data in list_price:
        if price_data.name is None:
            if price_data.price < last_price:
                last_price = price_data.price
        else:
            msg = msg + f'- <b>{price_data.name}:</b> {price_data.price} руб/мес\n'

    if last_price != MAX_PRICE:
        msg = msg + msg2.format(last_price)

    # msg = msg.replace('(', '\\(') \
    #     .replace(')', '\\)') \
    #     .replace('-', '\\-') \
    #     .replace('.', '\\.') \
    #     .replace('!', '\\!')

    f_tmp_image = tempfile.NamedTemporaryFile(mode='wb', delete=False)
    logging.debug(f'[BOT] Save poster to: {f_tmp_image.name}')
    download_image(kino_ext.poster_url, f_tmp_image)
    f_tmp_image.close()

    markup = telebot.types.InlineKeyboardMarkup()
    back_btn = telebot.types.InlineKeyboardButton(text='< Назад к списку', callback_data='1_' + db_row_id)
    view_btn = telebot.types.InlineKeyboardButton(text='Посмотреть', url=kinopoiskhd.get_url_film(film_id))
    markup.add(back_btn, view_btn)

    f_tmp_image = open(f_tmp_image.name, 'rb')
    bot.edit_message_media(chat_id=call.message.chat.id,
                           message_id=call.message.message_id,
                           media=telebot.types.InputMediaPhoto(media=f_tmp_image))
    f_tmp_image.close()
    os.remove(f_tmp_image.name)

    bot.edit_message_caption(chat_id=call.message.chat.id,
                             message_id=call.message.message_id,
                             caption=msg,
                             parse_mode='HTML',
                             reply_markup=markup)


def create_kino_markup(list_kino, db_row_id=None):
    markup = telebot.types.InlineKeyboardMarkup()

    for kino in list_kino:
        if db_row_id is not None:
            db_row_id2 = generate_random_id()
            database.table2[db_row_id2] = f'{kino.id}|{db_row_id}'
            kino.db_callback_data = '2_' + db_row_id2

        btn = telebot.types.InlineKeyboardButton(text=kino.title, callback_data=kino.db_callback_data)
        markup.add(btn)

    return markup


def callback_handler_back_to_list(call):
    db_row_id = call.data[2:]
    db_row = database.table1.get(db_row_id)

    msg = db_row.text
    list_kino = pickle.loads(db_row.result)
    markup = create_kino_markup(list_kino)

    f_image = open('./kinopoinshd_logo.png', 'rb')
    bot.edit_message_media(chat_id=call.message.chat.id,
                           message_id=call.message.message_id,
                           media=telebot.types.InputMediaPhoto(media=f_image))
    f_image.close()

    bot.edit_message_caption(chat_id=call.message.chat.id,
                             message_id=call.message.message_id,
                             caption=msg,
                             parse_mode='HTML',
                             reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    if call.data.startswith('2_'):
        callback_handler_kino_info(call)
    else:
        callback_handler_back_to_list(call)


@bot.message_handler(content_types=['text'])
def send_text(message: telebot.types.Message):
    logging.debug(f'[BOT] message obj: {message}')
    query = message.text.strip()

    list_kino = kinopoiskhd.search(query)
    logging.debug(f'[BOT] result list films: {list_kino}')

    reply_message_text = f'По запросу <i>\'{query}\'</i> '
    if len(list_kino) == 0:
        reply_message_text = reply_message_text + 'ничего не найдено =('
        bot.send_message(chat_id=message.chat.id,
                         text=reply_message_text,
                         parse_mode='HTML')
    else:
        reply_message_text = reply_message_text + 'найдено:'

        db_row_id = generate_random_id()

        markup = create_kino_markup(list_kino, db_row_id)

        # region Сохраняем в БД результаты поиска
        list_kino_serial = pickle.dumps(list_kino)
        dbrow = DbRow()
        dbrow.text = reply_message_text
        dbrow.result = list_kino_serial
        database.table1[db_row_id] = dbrow
        # endregion

        f_photo = open('./kinopoinshd_logo.png', 'rb')
        bot.send_photo(chat_id=message.chat.id,
                       photo=f_photo,
                       caption=reply_message_text,
                       parse_mode='HTML',
                       reply_markup=markup)
        f_photo.close()


# endregion

logging.info('Start bot bolling')
bot.polling()

logging.info('End program')
